package grupo2ficha3;
import java.util.Scanner;

public class Grupo2Ficha3 {

    public static void main(String[] args) {
        int c=0;
        double Vcilindro=0, Vesfera=0, Vcone=0, altura=0, raio=0;
        String solido;
        Scanner scanner= new Scanner(System.in);
        
        System.out.println("Escreva o sólido que quer calcular.");
        
        solido= scanner.next();
        System.out.println("solido="+solido.toString());
       
        
        while (c==0){
            if (solido.equals("esfera")){
                System.out.println("Insira o valor do raio");
                raio = scanner.nextDouble();
                Vesfera = (4*Math.PI*Math.pow(raio, 3))/3;
                System.out.println("O volume da esfera é: "+Vesfera);
                System.out.println("Escreva o novo sólido que quer calcular.");
                solido= scanner.next();
            } 
            if (solido.equals("cone")){
                System.out.println("Insira o Raio e a altura do cone.");
                raio= scanner.nextDouble();
                altura= scanner.nextDouble();
                Vcone=((Math.PI)*(Math.pow(raio, 2) )*(altura/3));
                System.out.println("O volume do cone é: "+Vcone);
                System.out.println("Introduza o novo sólido que quer calcular.");
                solido= scanner.next();
            }
            if (solido.equals("cilindro")){
                System.out.println("Insira o valor do raio");
                raio=scanner.nextDouble();
                System.out.println("Insira o valor da altura");
                altura=scanner.nextDouble();
                Vcilindro= (Math.PI)*(raio*raio)*altura;
                System.out.println("O volume do cilindro é "+Vcilindro);
                System.out.println("Escreva o novo sólido");
                solido= scanner.next();
            }
            if(solido.equals("FIM")){
                c=1;
            } 
        }
        
            
        
    }
    
}
